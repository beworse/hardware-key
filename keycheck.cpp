/*
 * Nazwa pliku:
 * keycheck.cpp
 * 
 * Data ostatniej modyfikacji:
 * 2016.09.15
 * 
 * Zastosowanie:
 * Program prównuje numer seryjny klucza sprzętowego o danym idVendor, idProduct (lsusb) oraz kluczu wygenerowanym funkcją: "nr seryjny" | md5sum .
 * 
 * Autor:
 * Andrzej Bartkowski <be.worse@gmail.com>
 * 
 * Kompilacja:
 * gcc keycheck.cpp `pkg-config --libs --cflags libusb-1.0` -lcrypto -o keycheck.out
 * 
 * Uwaga:
 * w niektorych przypadkach potrzebne sa uprawnienia administratora do uruchomienia - sudo ./keycheck.out
 */

#include <stdio.h>
#include <libusb.h>
#include <openssl/md5.h> //sudo apt-get install libssl-dev
#include <string.h>

int idVendor=0x0403;
int idProduct=0x6001;
int len = 256;

char key[] = "734eee1fdd7c739637d3fb831b1f717c"; // klucz do porownannia

int checkKey(char *b) //funkcja porownujaca klucz ze stigniem b 
{
    unsigned char digest[MD5_DIGEST_LENGTH + 1];
    //char string[] = ""; //bez tego nie dziala 
    
    MD5((unsigned char*)b, strlen(b), (unsigned char*)&digest);    
    
    char mdString[MD5_DIGEST_LENGTH];
 
    for(int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
         sprintf(&mdString[i*2], "%02x", (unsigned int)digest[i]);
    }
 
    //printf("md5sum:\n%s\n", mdString);
    
    if( strlen(key) == strlen(mdString) )
    {
        for( int i=0; i < strlen(mdString); i++ )
        {
            if( key[i] != mdString[i] )
            {
                //printf("klucze nie sa zgodne \n");
                return 0;
                
            }
        }
        
        return 1; //klucze zgodne
    }
    
    return 0;
    
};


int main() //glowna funkcja
{
//********** ZMIENNE **********//
    libusb_device **devs; //wskaznik na urzedzenia sluzy do stworzenia listy urzadzen
    libusb_device *dev; 
    
    libusb_context *ctx = NULL; //sesja libusb
    
    libusb_device_descriptor desc; //deskryptor
    libusb_device_handle  *devHandle = NULL;
    
    unsigned char strDesc[len];
    ssize_t cnt; //ilosc urzadzen w liscie
    int r; //do zwracania danych
    
//********** INICJALIZACJA LIBUSB **********//
    r = libusb_init(&ctx); //inicjalizacja libusb
    if( r < 0 )
    {
        printf ("Problem z inicjalizacja libusb\n");
        return -1;
    }
    
    
    cnt = libusb_get_device_list(ctx, &devs); //ile urzedzen wykryto
    
    for(int i = 0; i < cnt; i++) //petla do przeszukiwania listy urzadzen
    {
        r = libusb_open (devs[i], &devHandle); //otworzenie urzadzenia potrzebne aby dostac wykonac funkcje libusb_get_string_descriptor_ascii (devHandle)
        
        if( r < 0 )
        {
            printf ("Blad otwracia urzadzenia\n");
            return -1;
        }
        
        else
        {
            r = libusb_get_device_descriptor(devs[i], &desc); //otworzenie deskryptora urzadzenia potrzebne aby dostac sie do struktury desc
            
            if( r < 0 )
            {
                printf ("Blad dostepu do descryptora\n");
                return -1;
            }
            
            else
            {
                if( (desc.idVendor == idVendor) and (desc.idProduct == idProduct) ) //znaleziono klucz sprzetowy sprawdzenie jego numeru seryjnego
                {
                    libusb_get_string_descriptor_ascii(devHandle, desc.iSerialNumber, strDesc, len); //przeksztalcenie iSerialNumber do stringa kompatybilnego z c

                    
                    //printf ("numer seryjny: %s\n", strDesc);
                    
                    char *b = (char*) strDesc;
                    
                    r = checkKey(b);
                    
                    libusb_free_device_list(devs, 1); //zwolnienie listy urzadzen
                    libusb_exit(ctx); //zakonczenie sesji libusb
                    
                    if(r == 1)
                    {
                        printf("klucze zgodne\n");
                        return 0;
                    }
                    
                    printf("klucze nie zgodne\n");
                    return -1;
                }
            }
        }
    }
    
    printf("nie znaleziono klucza sprzetowego\n"); // o danym idVendor i idProduct
    libusb_free_device_list(devs, 1); //zwolnienie listy urzadzen
    libusb_exit(ctx); //zakonczenie sesji libusb
    return -1;
}
